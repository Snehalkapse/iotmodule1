# IoT (Internet of Things)
 - The Internet of things (IoT) is a system of interrelated computing devices, mechanical and digital machines provided with unique identifiers (UIDs) and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction

 ![](extras/iot.png)

# Industrial IoT
 ## What is Industrial IoT
 - The Industrial Internet of Things (IIoT) refers to interconnected sensors, instruments, and other devices networked together
 with computers' industrial applications, including manufacturing and energy management

 ![](extras/industrial_iot.jpg)

 # Architecture of Industrial IoT
  - ### Traditional Structure
  ![](extras/archi_of_iiot.png)
   
  - ### Mordern Structure

  ![](extras/archiOfmordeniiot.png)

# Revolution of Industrial IoT
 ![](extras/revolution_of_IIot.gif)

# Industry 1.0
 - The First Industrial Revolution began in the **18th century** through the use of **steam power and mechanisation of production**

 ![](extras/industry1.0.gif)

# Industry 2.0
 - The Second Industrial Revolution began in the **19th century** through the discovery of **electricity and assembly line production**. **Henry Ford (1863-1947)** took the idea of mass production from a slaughterhouse in **Chicago**

 ![](extras/industry2.0.gif)

 
# Industry 3.0
 
 - The Third Industrial Revolution began in the **70s** in the **20th century** through partial automation using **memory-programmable controls and computers**

 ![](extras/industy_3.0.jpg)

 - Since the introduction of these technologies, we are now able to automate an entire production process - without human assistance. Known examples of this are **robots** that perform programmed sequences without human intervention

 ![](extras/industry3.0.gif)

  - ## Protocols of Industry 3.0
  
   ![](extras/protocol_3.png)
   

# Industry 4.0
 - We are **currently** implementing the Fourth Industrial Revolution. 
 - This is characterised by the application of **information and communication technologies** to industry and is also known as "**Industry 4.0**". 
 - It builds on the developments of the Third Industrial Revolution. 

 ![](extras/IIoT4_overview.png)

 - Production systems that already have computer technology are expanded by a network connection and have a digital twin on the Internet so to speak. 
 - These allow communication with other facilities and the output of information about themselves. 
 - This is the next step in production automation. The networking of all systems leads to "**cyber-physical production systems**" and therefore smart factories, in which production systems, components and people communicate via a network and production is nearly autonomous.

 ![](extras/industry4.0.gif)

 - ## Protocols of Industry 4.0

  ![](extras/protocol_4.png)

 
# Difference between Industry 3.0 and Industry 4.0
 | Characteristics | Industry 3.0 | Industry 4.0 |
 | ------ | ------ | ------ |
 | Processes | Automation | Automation Decision making |
 | Industry Defining Technology | Industrial Robots | Collacorative Robots |
 | Production Planning | Demand Forecasting | On-Demand manufacturing |
 | Alignment | interconnection of Production Processes | Interconnection of the whole value chain |
 | Variation | Delimited Variation | individual unique product |
 | Goal | Efficiency | Flexibility |
 | Base for revenue model | selling product | Servitisation |


 ![](extras/industry_3.0_and_4.0.png)

# Problems with Industry 4.0

- **Cost** Changing Hardware means a big factory downtime, I dont want to shut down my factory to upgrade devices 
- **Downtime** I dont want to invest in devices which are unproven and unreliable.
- **Reliablity** I dont want to invest in devices which are unproven and unreliable.

# Solution :
 - Get data from Industry 3.0 devices/meters/sensors without changes to the original device. And then send the data to the Cloud using Industry 4.0 devices 
 - By changing industry 3.0 Protocols into industry 4.0 Protocols
 
## Converting Indusry 3.0 devices to Industry 4.0:
 To convert a 3.0 to 4.0, a library that helps, get data from industry 3.0 devices and send to industry 4.0 cloud.

 - Identify most popular industry 3.0 devices
 - Study protocols that these devices communicate
 - Get data from the industry 3.0 devices
 - Send the data to cloud for industry 4.0

# Tools Used:

 **After the data is sent to the cloud the following processes can take place**

 * **TSDB Tools** : Store data in _Time Shared Data database(TSDB)_ using tools like
    * Prometheus
    * InfluxDB
    * Things Board
    * Grafana

 * **IOT Dashboards** : View all your datas into beautiful dashboards.Iot dashboards available are:
    * Grafana 
    * Thingsboard


  ![](extras/iot_dashboard.png)

 * **IOT Platforms** : An IoT platform enables IoT device and endpoint management, connectivity and network management, data management, processing and analysis, application development, security, access control, monitoring, event processing and interfacing/integration. 

 ![](extras/iot_platform.png)

 * **Get Alerts/Notifications** : Gets alerts/SMS based on your data using these Platforms:   
   * Zaiper
   * Twilio






 



